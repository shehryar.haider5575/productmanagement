<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// ROUTE::Products
Route::get('/', 'Admin\ProductController@index')->name('product');
Route::get('/datatable', 'Admin\ProductController@datatable')->name('product.datatable');
Route::post('/store', 'Admin\ProductController@store')->name('product.store');
Route::get('/edit/{product}', 'Admin\ProductController@edit')->name('product.edit');
Route::put('/update/{product}', 'Admin\ProductController@update')->name('product.update');
Route::delete('/delete', 'Admin\ProductController@destroy')->name('product.delete');


Route::get('migrate',function(){
    \Artisan::call('migrate');
    return "Schema has been migrate!";
});

Route::get('clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
    Artisan::call('storage:link');
    return ('done');
});