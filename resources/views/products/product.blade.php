@extends('layouts.master')
@section('title', __('Products'))

@section('top-styles')
<link rel="stylesheet" href="{{url('')}}/dash-assets/plugins/sweetalert2/sweetalert2.min.css">
       
<!-- Plugins css-->
<link href="{{url('')}}/dash-assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" />
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{url('')}}/dash-assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

<!-- END PAGE LEVEL PLUGINS -->
<style>
    a.btn.btn-secondary {
        background: #dc3535bf;
        border: 1px solid white;
    }
    h2#swal2-title {
        font-size: 25px;
    }
    div#swal2-content {
        font-size: 17px;
    }
    .swal2-icon.swal2-warning, .swal2-success-ring {
        border-radius: 50% !important;
        font-size: 11px;
    }
    .swal2-popup.swal2-modal.swal2-icon-warning.swal2-show {
        height: 280px;
        width: 36em;
    }
    table.dataTable.no-footer
    {
        border-bottom:inherit !important; 
    }
</style>
@endsection

@section('content')
@section('breadcrumb')
<ul class="page-breadcrumb">
    <li>
        <span>{{ __('Products')}}</span>
    </li>
</ul>
@endsection

<!-- BEGIN Category STATS 1-->
<!-- BEGIN Products STATS 1-->
<div class="row">
    <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <i class="fa fa-map-marker"> </i> {{ __('Products')}}
                    </div>
                    <div class="panel-body">
                        <div class="custom_datatable">
                            <span class="text-danger" id="error"></span>
                            <form action="#" id="submitData">
                                <input type="hidden" name="token" id="token" value="{{csrf_token()}}">
                                <div class="bg-black-transparent1 m-b-15 p15 pb0">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">{{ __('Name')}}</label>
                                                <input type="text" name="name" id="autocomplete-ajax1"  class="form-control p_name" placeholder="Name" style="  z-index: 2;" utocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">{{ __('Quantity')}}</label>
                                                <input type="number" name="quantity" id="autocomplete-ajax1"  class="form-control p_quantity" placeholder="quantity" style="  z-index: 2;" utocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">{{ __('Price')}}</label>
                                                <input type="text" name="price" id="autocomplete-ajax1"  class="form-control p_price" placeholder="price" style="  z-index: 2;" utocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <br>
                                            <div class="form-group" style="margin-top: 5px">
                                                <button id="submit" type="button" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                <i class="fa fa-save pr-1"></i> {{ __('Submit') }}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                 </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th width="7%" class="all">{{ __('S.No')}}</th>
                                                <th class="min-phone-l">{{ __('Name')}}</th>
                                                <th class="min-phone-l">{{ __('Quantity')}}</th>
                                                <th class="min-phone-l">{{ __('Price')}}</th>
                                                <th class="min-phone-l">{{ __('Total Value Number')}}</th>
                                                <th class="all" width="10%">{{ __('Actions')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END VALIDATION STATES-->
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection

@section('rightsidebar')
@parent
@endsection

@section('page-scripts')
{{-- SweetAlert2 --}}
<script src="{{url('')}}/dash-assets/plugins/sweetalert2/sweetalert2.min.js" charset="UTF-8"></script>

<script src="{{url('')}}/dash-assets/plugins/switchery/js/switchery.min.js"></script>
<script src="{{url('')}}/dash-assets/plugins/custom/jquery.nicescroll.js"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{url('')}}/dash-assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{url('')}}/dash-assets/pages/scripts/table-datatables-responsive.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
{{-- PDF MAKE --}}
{{-- <script src="{{url('')}}/dash-assets/plugins/pdfmake/pdfmake.min.js"></script>
<script src="{{url('')}}/dash-assets/plugins/pdfmake/vfs_fonts.js"></script>
<script src="{{url('')}}/dash-assets/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="{{url('')}}/dash-assets/plugins/datatables-buttons/js/buttons.print.min.js"></script> --}}
@endsection

@section('custom-script')
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#sample_1').DataTable({
            // retrieve: true,
            destroy: true,
            processing: true,
            serverSide: true,
            ajax: '{{route("product.datatable")}}',
            "columns": [{
                    "data": "id",
                    "defaultContent": ""
                },
                {
                    "data": "name",
                    "defaultContent": ""
                },
                {
                    "data": "quantity",
                    "defaultContent": ""
                },
                {
                    "data": "price",
                    "defaultContent": ""
                },
                {
                    "data": "id",
                    "defaultContent": ""
                },
                {
                    "data": "id",
                    "defaultContent": ""
                },
            ],
            "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                },
                {
                    "targets": 0,
                    "render": function (data, type, row, meta) {
                        return meta.row + 1;
                    },
                },
                {
                    "targets": -2,
                    "render": function (data, type, row, meta) {
                        return row.quantity * row.price;
                        ;
                           // <span style='font-size:16px;font-weight:bolder;'>View</span>
                    },
                },
                {
                    "targets": -1,
                    "render": function (data, type, row, meta) {
                    return `
                        <a href="javascript:;" class="delete text-danger p-2" data-original-title="Delete" title="" data-placement="top" data-toggle="tooltip" data-id="` + row.id + `" style="padding-right:6px;">
                            <i class="fa fa-trash-o"></i>
                        </a>
                        `;
                    },
                },
            ],
            "drawCallback": function (settings) {
                $('.delete').click(function () {
                    var deleteId = $(this).data('id');
                    var $this = $(this);

                    Swal.fire ({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        type: 'warning',
                        icon: 'warning',

                        showCancelButton: true,
                        confirmButtonColor: '#4fa7f3',
                        cancelButtonColor: '#d57171',
                        confirmButtonText: 'Yes, delete it!'
                    })
                       .then(function (result) {
                        if (result.value) {
                            axios
                                .post('{{route("product.delete")}}', {
                                    _method: 'delete',
                                    _token: '{{csrf_token()}}',
                                    id: deleteId,
                                })
                                .then(function (response) {
                                    console.log(response);

                                    Swal.fire(
                                        'Deleted!',
                                        'Product has been deleted.',
                                        'success'
                                    )

                                    table
                                        .row($this.parents('tr'))
                                        .remove()
                                        .draw();
                                })
                                .catch(function (error) {
                                    console.log(error);
                                    Swal.fire(
                                        'Failed!',
                                        error.response.data.error,
                                        'error'
                                    )
                                });
                        }
                    })
                });
                $('#submit').click(function (e) {
                    var name        =   $('.p_name').val();
                    var quantity    =   $('.p_quantity').val();
                    var price       =   $('.p_price').val();
                    var $this       =   $(this);

                    axios
                        .post('{{route("product.store")}}', {
                            _method: 'post',
                            _token: '{{csrf_token()}}',
                            name: name,
                            quantity: quantity,
                            price: price,
                        }).then(function(res){
                        swal(
                        'Added!',
                        'You Product has been created.',
                        'success'
                        )
                        table.draw();

                    }).catch(function(err){
                        swal(
                        'Failed!',
                        err.message,
                        'error'
                        )
                        console.log(err);
                    });
                    table.draw();

                });

            },
            //scrollX:true,
        });
       
        // table.draw();
        // $(".custom_datatable #DataTable_wrapper .row:nth-child(2) .col-sm-12").niceScroll();
    });
</script>
@endsection