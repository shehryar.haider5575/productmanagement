<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Admin;
use App\Product;
use DataTables;
use Storage;
use Auth;
use Hash;
use DB;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $data = [
            'total_products'    =>  Task::get(),
        ];
        return view('dashboard',$data);
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function profile()
    {
        $data = [
            'user'    =>  Admin::first(),
        ];
        return view('profile.setting',$data);
    }
    public function setting( Request $request)
    {
        // return $request->all();
        $request->validate([
            'name'              => 'required|string|max:100',
            'email'             => "required|max:255|unique:users,email,".Admin::first()->id,
            'current_password'  => 'required_if:change_password,1',
            'password'          => 'required_if:change_password,1|confirmed|min:6|max:22',
        ]);

        if($request->change_password == 1 && !Hash::check($request->current_password, Admin::first()->password))
        {
            return redirect()->back()->withErrors(['current_password'=>'your current password does not match'])->withInput($request->all());

        }

        $input_data = $request->except('password', 'password_confirmation','_token','_method','change_password','current_password');

        if($request->password)
        {
            $input_data['password'] = bcrypt($request->input('password'));
        }

        Admin::first()->update($input_data);

        return redirect()->route('home');
    }
}
