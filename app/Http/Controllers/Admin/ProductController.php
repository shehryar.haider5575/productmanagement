<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use DataTables;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('products.product');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable()
    {
        $product = Product::orderBy('created_at','desc')->select(['id','name','quantity','price']);

        return DataTables::of($product)->make();
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store( Request $request)
    {
        $data = $request->validate([
            'name'          =>  'required|unique:products,name',
            'quantity'      =>  'required|numeric',
            'price'         =>  'required',
        ]);
        Product::create($data);
        
        $response['message'] = 'success';
        $response['data']    = 'Product Successfully Created';
        return response()->json($response, 200);

    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Product $product)
    {
        $response['message'] = 'success';
        $response['data']    = $product;
        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, Product $product)
    {
        $data = $request->validate([
            'name'          =>  'required|unique:products,name,'.$product->id,
            'quantity'      =>  'required|numeric',
            'price'         =>  'required',
        ]);
       
        $product->update($data);
        
        $response['message'] = 'success';
        $response['data']    = $product;
        return response()->json($response, 200);
    }


     /**
     * Remove
     *  the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $product = Product::findOrFail($request->id);
        // apply your conditional check here
        if ($product->delete()) {
            $response['success'] = 'Product Successfully Deleted.';
            return response()->json($response, 200);
        } else {
            // Storage::disk('uploads')->delete($product->image);
            $response['error'] = 'Oops! Something went wrong.';
            return response()->json($response, 409);
        }
    }
}
